define(['jquery', 'uikit', 'react', 'react-dom'], function($, UI, React, ReactDOM) {

  Reactor.Views.Slider = React.createClass({

    juxtaposeSlider: function(id, params){
      var images = $("#"+id+" img"),
          j_images = [],
          self = this;

      $(images).each(function(){
        var _this = $(this);
        j_images.push({
          src: siteurl+'/src/img/16x9.gif',
          label: '',
          credit: ''
        });
        
        _this.remove();
        //_this.wrap('<div class="corners"></div>');
      });

      var slider = new juxtapose.JXSlider('#'+id, j_images, params);      
      return slider;
    },

    componentDidMount: function() {
      if (this.state == 'imagesReady') {
        var self = this;
        
        var slider = this.juxtaposeSlider(self.props.id, {
              animate: true,
              showLabels: false,
              showCredits: false,
              startingPosition: self.props.startingPosition ? self.props.startingPosition : self.props.centerBar ? "50%" : "25%",
              makeResponsive: true,
              callback: function() {
                $("#"+self.props.id).find(".jx-image").each(function(index){
                  $(this).wrapInner('<div class="jx-skew" />');

                  if (!self.props.images[index]['visibility']) {
                    $(this).closest(".juxtapose").addClass("dark-bar");
                  } else {
                      $(this).find("img").css("background-image", "url("+self.props.images[index]['path']+")");
                    }

                  var titleTag = self.props.titleTag ? self.props.titleTag : "h1";
                  $(this).find(".jx-skew").append('<'+titleTag+' style="width: '+self.props.titleWidth+'px;">'+self.props.title+'</'+titleTag+'>');
                  var wrapper = $(this).closest(".jx-wrap");

                  self.props.inversed ? wrapper.addClass("skew-inversed") : "";
                  self.props.corners ? wrapper.addClass("trimmed") : "";
                  self.props.autoRefresh ? wrapper.addClass("auto-refresh") : "";
                  self.props.hideHandle ? $("#"+self.props.id).find(".jx-controller").hide() : ""
                  self.props.shift ? wrapper.css("float", self.props.shift) : "";

                  self.adjustSliderLayout(self.props.titleWidth);
                });                
              }
          });

        window.addEventListener('resize', this.handleResize);
        this.state = "sliderRendered";
      }
    },

    getTangens: function(degrees) {
      with (Math) {        
        return sin(degrees*PI/180)/cos(degrees*PI/180);
      }      
    },    

    adjustSliderLayout: function(width){
      var self = this,
          _margin = parseFloat($("#"+this.props.id).width()/2) + parseFloat(width/2);

      $("#"+this.props.id).find(".jx-image").each(function(){      
        var title = $(this).find("h1"),
            padding = "",
            _padding = parseFloat($("#"+self.props.id).height())*self.getTangens(10)/2;

        title.css("margin-top", "-"+parseFloat(title.height()/2)+"px");

        if ($(this).hasClass("jx-left")) {
          $(this).find("img").css("left", _padding*(-1)+"px");

          title.css("margin-right", "-"+(_margin - (self.props.centerBar ? _padding : $("#"+self.props.id).width()/6))+"px");
        } else {
            $(this).find("img").css("right", _padding*(-1)+"px");

            title.css("margin-left", "-"+(_margin + (self.props.centerBar ? _padding : $("#"+self.props.id).width()/6))+"px");
          }

        if (self.props.fullwidth) {

          var middleScreen = parseFloat($("#"+self.props.id).width()/2);

          if ($(this).hasClass("jx-left")) {
            $(this).css({
              "margin-left": "-"+_padding+"px",
              "padding-left": _padding+"px"
            });

            title.css("margin-right", "-"+(middleScreen + _padding + self.props.titleWidth/2) +"px");

          } else {
              $(this).css({
                "margin-right": "-"+_padding+"px",
                "padding-right": _padding+"px"
              });

              title.css("margin-left", "-"+(middleScreen + _padding + self.props.titleWidth/2) +"px");
            }

        }

        /**/


        /*if (self.props.centerBar) {
          
        }*/
      });
    },

    handleResize: function(e) {
      var self = this;
      if (this.state == 'sliderRendered') {
        setTimeout(function() {
          self.adjustSliderLayout(self.props.titleWidth);
        }, 50);
      }
    },

    sliderImages: function(images) {
      if (images.length) {
        var i = 0,
            DOMimages = [],
            DOMimage = {};
        do {

          DOMimages.push(React.createElement('img', {
            src: images[i].path
          }));

          i += 1;
        } while (i < images.length);

        return DOMimages;
      }
    	
      return false;
    },

    render: function() {
      var images = this.sliderImages(this.props.images);      
      if (images) {
        this.state = 'imagesReady';

        return React.createElement('div', { id: this.props.id }, images[0], images[1]);
      } else {
          this.state = 'ready';
          return false;
        }      
    }
  });
});