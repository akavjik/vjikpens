define(['jquery', 'uikit', 'react', 'react-dom'], function ($, UI, React, ReactDOM) {

    $(document).ready(function () {
        setTimeout(function () {
            $("body").addClass("loaded");
        }, 500);
    });

    var jxSliders = [
                      {
                          'id': 'juxtapose-1',
                          'images': [{
                              path: siteurl + '/src/img/past.jpg',
                              visibility: true
                          },

                                      {
                                          path: siteurl + '/src/img/future.jpg',
                                          visibility: true
                                      }
                          ],
                          'title': 'Wij richten <br>ons leven in <br>zoals wij <br>dat willen',
                          'titleWidth': 540,
                          'centerBar': true,
                          'corners': true,
                          'inversed': false,
                          'startingPosition': '99.999%',
                          'fullwidth': true
                      },

                      {
                          'id': 'juxtapose-2',
                          'images': [{
                              path: siteurl + '/src/img/past.jpg',
                              visibility: false
                          },

                                      {
                                          path: siteurl + '/src/img/future.jpg',
                                          visibility: true
                                      }
                          ],

                          'title': 'Wat Betekent <br>DIT VOOR <br>JOUW PENSIOEN?',
                          'titleWidth': 540,
                          'centerBar': false,
                          'corners': false,
                          'inversed': false
                      },

                      {
                          'id': 'juxtapose-3',
                          'images': [{
                              path: siteurl + '/src/img/past.jpg',
                              visibility: false
                          },

                                      {
                                          path: siteurl + '/src/img/man.jpg',
                                          visibility: true
                                      }
                          ],

                          'title': 'Wat Betekent <br>DIT VOOR <br>JOUW PENSIOEN?',
                          'titleWidth': 540,
                          'centerBar': false,
                          'corners': false,
                          'inversed': true
                      },

                      {
                          'id': 'juxtapose-4',
                          'images': [{
                              path: siteurl + '/src/img/cafe.jpg',
                              visibility: true
                          },

                                      {
                                          path: siteurl + '/src/img/fish.jpg',
                                          visibility: true
                                      }
                          ],

                          'title': '',
                          'titleWidth': 0,
                          'centerBar': true,
                          'corners': false,
                          'inversed': true,
                          'autoRefresh': true,
                          'autoRefreshSpeed': 500
                      },

                      {
                          'id': 'juxtapose-5',
                          'images': [{
                              path: siteurl + '/src/img/old.jpg',
                              visibility: true
                          },

                                      {
                                          path: siteurl + '/src/img/guy.jpg',
                                          visibility: true
                                      }
                          ],

                          'title': 'We worden <span>steeds ouder</span>',
                          'titleTag': 'h6',
                          'titleWidth': 0,
                          'centerBar': true,
                          'corners': false,
                          'inversed': true,
                          'hideHandle': true,
                          'shift': 'left'
                      },

                      {
                          'id': 'juxtapose-6',
                          'images': [{
                              path: siteurl + '/src/img/rest.jpg',
                              visibility: true
                          },

                                      {
                                          path: siteurl + '/src/img/work.jpg',
                                          visibility: true
                                      }
                          ],

                          'title': '',
                          'titleTag': 'h6',
                          'titleWidth': 0,
                          'centerBar': true,
                          'corners': false,
                          'inversed': false,
                          'hideHandle': true,
                          'shift': 'right'
                      }
    ];

    var factory = React.createFactory(Reactor.Views.Slider);

    $.each(jxSliders, function (index) {
        var target = document.getElementById('jx-slider-' + (index + 1));
        if (target) {
            $(target).wrap('<div class="jx-wrap"></div>');
            ReactDOM.render(factory(this), target);
        }
    });
});