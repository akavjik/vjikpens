﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using Umbraco.Core.Logging;

namespace PensioenfederatieSite.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new StyleBundle("~/bundles/css").Include(
            //    "~/css/screen.css"));

            //bundles.Add(new ScriptBundle("~/bundles/js").Include(
            //    "~/scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts").IncludeDirectory(
                "~/scripts", "*.js", true));


            bundles.Add(new StyleBundle("~/bundles/css").IncludeDirectory(
                "~/css", "*.css", true));



            LogHelper.Info<string>("Bundles Loaded");

            //Comment this out to control this setting via web.config compilation debug attribute
            BundleTable.EnableOptimizations = true;
        }
    }
}