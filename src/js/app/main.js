var appPath = 'app/',
    mainPoint = 'main',
    config = {
      paths: {
        'index': appPath+'index/'+mainPoint,
        'reactor': appPath+'index/react',
        'core': appPath+'index/components/core'
      },

      shim: {
        'core': {},
        'index': {deps: ['core']}        
      }
    }

require.config(config);
require(Object.keys(config.shim));