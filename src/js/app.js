var vendorPath = '../../vendor/',
    config = {
      paths: {
        'jquery': vendorPath+'jquery/jquery.min',
        'uikit': vendorPath+'uikit/js/uikit',
        'juxtapose': vendorPath+'juxtapose/build/js/juxtapose.min',
        'onepage-scroll': vendorPath+'onepage-scroll/jquery.onepage-scroll.min',
        'react': vendorPath+'react/react',
        'react-dom': vendorPath+'react/react-dom',
        'reactor': 'app/reactor',
        'main': 'app/main'
      },

      config: {
        'uikit': {
          'base': vendorPath+'uikit/js/'
        }
      },

      shim: {
        'react': {},
        'react-dom': {},
        'uikit': {deps: ['jquery']},
        'uikit!slideshow-fx': {deps: ['uikit!slideshow']},
        'juxtapose': {deps: ['jquery']},
        'onepage-scroll': {deps: ['jquery']},
        'reactor': {},
        'main': {}
      }
    }

require.config(config);
require(Object.keys(config.shim));