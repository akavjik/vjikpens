﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace PensioenfederatieSite.Controllers
{
    public class PollController : SurfaceController
    {

        [HttpPost]
        public ActionResult Submit(RenderModelBinder model)
        {

            string result = "";

            if (!Request.Form.AllKeys.Contains("answerlist"))
            {
                return Content("Make your chose in poll");
            }

            int answernodeid = Convert.ToInt32(Request.Form.GetValues("answerlist")[0]);
            var cs = Services.ContentService;

            var answernode = cs.GetById(answernodeid);
            var pollnode = cs.GetById(answernode.ParentId);
            int allvote = 0;

            int votes = Convert.ToInt32(answernode.GetValue("vote"));
            votes = ++votes;
            answernode.SetValue("vote", votes);
            cs.SaveAndPublishWithStatus(answernode);


            foreach (var item in cs.GetChildren(pollnode.Id))
            {
                result = result + (String.Format("answer {0}) votes {1}", item.Id.ToString(), (Convert.ToInt32(item.GetValue("vote")).ToString())));
                allvote = allvote + Convert.ToInt32(item.GetValue("vote"));
            }

            result = result + "  Totale votes =" + allvote;

            return Content(result);

        }



    }
}