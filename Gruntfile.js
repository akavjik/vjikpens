module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    less: {
      development: {
/*        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },*/
        files: {
            "css/app.css": "src/css/app.less" // destination file and source file
        }
      }
    },
    watch: {
      styles: {
        files: ['src/**/*.less', 'vendor/**/*.less'], // which files to watch
        tasks: ['less'],
        options: {
          nospawn: true
        }
      },
      livereload: {
        files: ['css/app.css'],
        options: {
          livereload: true,
        }
      }
    }
  });


  grunt.loadNpmTasks('grunt-contrib-uglify');
  // grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');  

  grunt.loadNpmTasks('grunt-hash');
  grunt.loadNpmTasks('grunt-contrib-clean');

  grunt.loadNpmTasks("grunt-strip");

  // Default task(s).
  grunt.registerTask('default', ['watch']);
  //grunt.registerTask('buildjs', ['clean:jsbuild', 'requirejs', 'strip', 'hash']);
  //grunt.registerTask('buildjs-dev', ['clean:jsbuild', 'requirejs', 'hash']);
  //grunt.registerTask('deploy', ['buildjs', 'sass:backend', 'sass:prod', 'sass:prodSmp']);
}


